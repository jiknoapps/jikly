function avatar(message) {
	return message.author.avatarURL;
}

module.exports = avatar;
