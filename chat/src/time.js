function time() {
	let hour = new Date().getHours();
	hour = hour > 12 ? hour - 12 : hour;
	const min = new Date().getMinutes();
	const m = hour >= 12 ? 'PM' : 'AM';
	return `${hour}:${String(min).padStart(2, 0)} ${m}`;
}

module.exports = time;
