function hello(message) {
	return `Hi, ${message.author.username}!`;
}

module.exports = hello;
