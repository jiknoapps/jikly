function catchAll(message) {
	message.react('🤔');
	return `Sorry , ${message.author} I am not sure what you are talking about!`;
}

module.exports = catchAll;
