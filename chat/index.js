const time = require('./src/time');
const avatar = require('./src/avatar');
const catchAll = require('./src/catch-all');
const hello = require('./src/hello');

module.exports = {
	time,
	avatar,
	catchAll,
	hello,
};
