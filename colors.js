//     error-color: #00a331;
//     warn-color: #c00606;
//     hint-color: #ffc400;
//     green-color: #00a2ff

module.exports = {
	info: 0x00a331,
	error: 0xc00606,
	warn: 0xffc400,
	hint: 0x00a2ff,
};
