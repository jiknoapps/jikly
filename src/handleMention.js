const chat = require('../chat/index');

function handleMention(message) {
	const reg = createRegExp();
	let command = filterCommand(message.content);

	if (reg.hello.test(command)) send(message, chat.hello(message));
	if (reg.avatar.test(command)) send(message, chat.avatar(message));
	if (reg.time.test(command)) send(message, chat.time());
	else send(message, chat.catchAll(message));
}

module.exports = handleMention;

// HELPER FUNCTIONS - START

// Take mentions out of the command
function filterCommand(command) {
	command = command.replace(/<@626946474915266579>/, '');
	command = command.trim();
	command = command.toLowerCase();
	return command;
}

// Send a messgage
function send(message, value) {
	message.channel.send(value);
}

// Create the regular expressions
function createRegExp() {
	return {
		hello: /.*(hey|hi|hello|yo|whats up|hey|howdy).*/i,
		avatar: /.*(what|where|whats).*avatar.*/i,
		time: /.*(what|where|whats).*time.*/i,
	};
}
