const msgLog = require('debug')('jikly:message');
const error = require('debug')('jikly:error');
const fs = require('fs');
const callCommands = require('../commands/index');
const pathToDatabase = './database';

function handleMessage(message) {
	const command = findCommand(message.content);
	if (!command) return;

	try {
		return send(message, command);
	} catch (err) {
		error('Message Failed: \n\n', err);
	}
}

function send(message, command) {
	message.react('✅');
	msgLog('Sending message...');
	message.channel.send(callCommands[command.command](message));
	msgLog('Message Sent!');
	return true;
}

function findCommand(message) {
	return getCommands().find((val) => {
		return '!' + val.command === message.split(' ')[0];
	});
}

function getCommands() {
	const json = fs.readFileSync(`${pathToDatabase}/commands.json`, 'utf-8');
	return JSON.parse(json);
}

module.exports = handleMessage;
