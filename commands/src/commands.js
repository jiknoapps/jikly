const { RichEmbed } = require('discord.js');
const { info, error } = require('../../colors');
const fs = require('fs');
const pathToDatabase = './database';

function commands(message) {
	const mod = getModules().find((val) => {
		let moduler = message.content.split(' ')[1];

		return val.module === moduler.toLowerCase();
	});

	const embed = new RichEmbed()
		.setTitle('Commands')
		.setColor(mod ? info : error)
		.setDescription('Type `!help <commandName> to get more information`')
		.addField(`Commands in ${mod.module}`, mod.commands);

	return embed;
}

function getModules() {
	const json = fs.readFileSync(`${pathToDatabase}/modules.json`, 'utf-8');
	return JSON.parse(json);
}

module.exports = commands;
