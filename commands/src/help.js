const { RichEmbed } = require('discord.js');
const { info } = require('../../colors');

const modules = `
Support
Users
Credits
Administration
`;
function help() {
	const embed = new RichEmbed()
		.setTitle('Jikly Help')
		.setColor(info)
		.setDescription(
			'Type `!commands <moduleName>` to get all the commands in that module.'
		)
		.addField('Modules', modules);
	return embed;
}

module.exports = help;
