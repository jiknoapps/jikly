function avatar(message) {
	const person = message.mentions.users.first();
	if (!person) return message.author.avatarURL;
	else return person.avatarURL;
}

module.exports = avatar;
