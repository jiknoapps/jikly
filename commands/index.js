const help = require('./src/help');
const commands = require('./src/commands');
const avatar = require('./src/avatar');

module.exports = {
	help: help,
	commands: commands,
	avatar: avatar,
};
