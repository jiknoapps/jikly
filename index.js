const { Client } = require('discord.js');
const start = require('debug')('jikly:startup');
const msgLog = require('debug')('jikly:message');
const messageHandler = require('./src/handleMessage.js');
const mentionHandler = require('./src/handleMention');

// Create a new client
const client = new Client();

client.on('ready', () => {
	start('Client Ready!');
});

client.on('message', (message) => {
	if (message.author.tag === 'Jikly#7762') {
		console.log('I sent', message.content);
		return;
	}
	msgLog(message.author.username, 'Wrote:', message.content);

	const reg = /.*<@626946474915266579>.*/;
	if (!messageHandler(message) && reg.test(message.content)) {
		mentionHandler(message);
	}
});

// Login to the bot
client
	.login('NjI2OTQ2NDc0OTE1MjY2NTc5.XY4iwA.vtgYqhFt6zsxxRdyM3HIQp2cgiE')
	.then(() => start('Logged in as:', client.user.tag));
